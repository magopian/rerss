"Simple ReRSS python server."
from pathlib import Path

from setuptools import find_packages, setup

VERSION = (0, 1, 0)

__author__ = 'Yohan Boniface'
__contact__ = "yohan.boniface@data.gouv.fr"
__homepage__ = "https://github.com/yohanboniface/rerss"
__version__ = ".".join(map(str, VERSION))

setup(
    name='rerss',
    version=__version__,
    description=__doc__,
    long_description=Path(__file__).parent.joinpath('README.md').read_text(),
    url=__homepage__,
    author=__author__,
    author_email=__contact__,
    license='WTFPL',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Developers',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='rss rerss atom',
    packages=find_packages(exclude=['tests']),
    extras_require={'test': ['pytest'], 'docs': 'mkdocs'},
    include_package_data=True,
)
