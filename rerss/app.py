import re
from datetime import datetime, timezone
from urllib.parse import unquote

import aiohttp
import ujson as json
import xmltodict
from cssselect import HTMLTranslator
from lxml.html import fromstring, tostring
from lxml.html.clean import clean_html
from roll import Roll
from roll.extensions import cors, traceback

from .models import Entry, Feed

# Idea add extracts

app = Roll()
cors(app)
traceback(app)

# TODO: make me configurable.
# We don't attach this to the feed itself, because in ReRSS world a RSS
# can contain any origin source.
SELECTORS = {
    r'^https?://(www.)?lemonde.fr': '#articleBody',
}

SELECTORS = {re.compile(p): HTMLTranslator().css_to_xpath(s)
             for p, s in SELECTORS.items()}


async def fetch_content(link):
    for pattern, selector in SELECTORS.items():
        if pattern.match(link):
            async with aiohttp.ClientSession() as session:
                async with session.get(link) as resp:
                    content = await resp.text()
                    doc = fromstring(content)
                    try:
                        node = doc.xpath(selector)[0]
                    except IndexError:
                        return
                    return clean_html(tostring(node))


@app.route('/sync', protocol='websocket')
async def sync(req, ws):
    from .cli import sync
    total = Feed.select().where(Feed.active == True).count()
    done = 0
    async for feed in sync():
        done += 1
        await ws.send(json.dumps({'progress': done / total}))
    await ws.send(json.dumps({'status': 'done'}))


@app.route('/feed')
async def feeds(req, resp):
    resp.json = {'feeds': [feed.json for feed
                           in Feed.select().order_by(Feed.title)]}


@app.route('/feed', methods=['POST'])
async def new_feed(req, resp):
    payload = json.loads(req.body.decode())
    feed = await Feed.from_link(payload['link'])
    resp.json = feed.json


@app.route('/feed/{link:path}', methods=['GET', 'POST'])
async def feed(req, resp, link):
    feed = Feed.get(Feed.link == unquote(link))
    if req.method == 'POST':
        payload = json.loads(req.body.decode())
        for key, value in payload.items():
            setattr(feed, key, value)
        feed.save()
    resp.json = feed.json


@app.route('/entry')
async def entries(req, resp):
    limit = req.query.int('limit', 20)
    offset = req.query.int('offset', 0)
    qs = Entry.select()
    seen = req.query.bool('seen', None)
    if seen is not None:
        qs = qs.filter(Entry.seen == seen)
    bookmark = req.query.bool('bookmark', None)
    if bookmark is not None:
        qs = qs.filter(Entry.bookmark == bookmark)
    qs = qs.order_by(Entry.updated.desc()).offset(offset).limit(limit)
    resp.json = {'entries': [entry.json for entry in qs]}


@app.route('/entry/{link:path}')
async def entry(req, resp, link):
    link = unquote(link)
    entry = Entry.get(Entry.link == link)
    # if not entry.content:
    #     content = await fetch_content(entry.link)
    #     if content:
    #         entry.content = content
    #         entry.save()
    resp.json = entry.json


@app.route('/entry/{link:path}', methods=['POST'])
async def update_entry(req, resp, link):
    link = unquote(link)
    payload = json.loads(req.body.decode())
    payload.pop('sources', None)  # M2M.
    Entry.update(**payload).where(Entry.link == link).execute()
    entry = Entry.get(Entry.link == link)
    resp.json = entry.json


@app.route('/myfeed/atom')
async def myfeed(req, resp):
    feed = {
        'feed': {
            '@xmlns': 'http://www.w3.org/2005/Atom',
            'title': 'ReRss',
            'link': req.path,
            'updated': datetime.now(timezone.utc),  # Fixme.
            'entry': []
        }
    }
    entries = (Entry.select().where(Entry.flagged == True)
                    .order_by(Entry.updated.desc())
                    .limit(15))
    for entry in entries:
        feed['feed']['entry'].append({
            'title': entry.title,
            'summary': entry.summary,
            'updated': entry.updated,
            'link': {
                '@href': entry.link
            }
        })
    resp.body = xmltodict.unparse(feed)
    resp.headers['Content-Type'] = 'application/xml'
