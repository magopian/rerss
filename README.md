# ReRSS Python server


## Install

    python setup.py develop


## Run locally

    python rerss/app.py initdb
    python rerss/app.py serve

Then browse to http://localhost:1707/app/index.html
